const addButton = document.querySelector("#push");
const addTaskInp = document.querySelector(".addTaskInp");
const toDoList = document.querySelector(".toDoList");

let currentToDoList = [];
let completedToDoList = [];

document.addEventListener("DOMContentLoaded", () => {
	currentToDoList = getFromLocalStorage("currentToDoList");
	renderToDoList(currentToDoList);
});

addButton.addEventListener("click", addToDo);

//Функция, которая запуская по нажатию на кнопку addButton
function addToDo() {
	// 1. Проверяем значение из инпута на пустую строку, если пустая строка, то выводим алерт
	if (addTaskInp.value.trim() == "") {
		alert("Please Enter a Task");
	}
	//Если строка не пустая, то:
	//2. Создаем переменную, в ней вызываем функцию, которая создаст объект с данными
	//3. Запушим созданный объект в массив
	//4. Сохраним массив в LS
	//5. Аппендим в список ul элемент, который тут же создаем
	//6. Очищаем поле ввода
	else {
		const toDo = createToDoData();
		currentToDoList.push(toDo);
		setToLocalStorage("currentToDoList", currentToDoList);
		toDoList.append(createEl(toDo.id, toDo.complete, toDo.title));
		addTaskInp.value = "";
	}
}

function renderToDoList(arr) {
	const toDoElements = arr.map((toDo) => {
		return createEl(toDo.id, toDo.complete, toDo.title);
	});
	if (toDoList.hasChildNodes()) {
		toDoList.innerHTML = "";
	}
	toDoList.append(...toDoElements);
}

//Функция, которая создаст объект с данными для элемента разметки HTML
function createToDoData() {
	return {
		id: Date.now(),
		complete: false,
		title: addTaskInp.value,
	};
}

function setToLocalStorage(key, arr) {
	localStorage.setItem(key, JSON.stringify(arr));
}

function getFromLocalStorage(key) {
	const dataFromLS = JSON.parse(localStorage.getItem(key));
	return dataFromLS != null ? dataFromLS : [];
}

//Функция, которая создает элемент для разметки HTMLн основе объекта из функции createToDoData
function createEl(id, complete, title) {
	const el = document.createElement("li");
	el.className = "toDoList__item";
	el.dataset.id = id;
	el.draggable = true;

	el.innerHTML = `
	<div class="toDoList__item-container">
	<input type="checkbox" class="item-checkbox" ${complete ? "checked" : ""}>
	<p class="toDoList__item-text">${title}</p>
	<button class="btn_delete"><img class="deleteIcon" src="./trashcan.svg"/></button>
	</div>
	`;

	const btnDelete = el.querySelector(".btn_delete");
	btnDelete.addEventListener("click", (event) => {
		deleteElement(event.currentTarget.closest(".toDoList__item"));
	});

	const inpCheckbox = el.querySelector(".item-checkbox");
	inpCheckbox.addEventListener("change", changeComplete);

	return el;
}

//Функция для удаления элемента
function deleteElement(delEl) {
	const elId = delEl.dataset.id;
	currentToDoList = currentToDoList.filter((el) => el.id != elId);
	setToLocalStorage("currentToDoList", currentToDoList);
	delEl.remove();
}

function changeComplete(event) {
	const checkedEl = event.currentTarget.closest(".toDoList__item");
	const elId = checkedEl.dataset.id;

	currentToDoList = currentToDoList.map((el) => {
		if (el.id == elId) {
			el.complete = event.currentTarget.checked;
		}
		return el;
	});

	setToLocalStorage("currentToDoList", currentToDoList);
}

addTaskInp.addEventListener("keyup", (event) => {
	if (event.keyCode == 13) {
		addToDo();
	}
});
